import React from "react";
import "./Home.css";

export default function Home() {
  return (
    <div className="Home">
      <div className="lander">
        <h1>New Quote</h1>
        <p>A simple quote pre-processor page</p>
      </div>
    </div>
  );
}