import * as dynamoDbLib from "../lib/dynamodb-lib";
import { success, failure } from "../lib/response-lib";

export async function main(event, context) {
    const params = {
        TableName: process.env.quotes_tablename,
        Key: {
            userId: event.requestContext.identity.cognitoIdentityId,
            quoteId: event.pathParameters.id
        }
    };

    try {
        const result = await dynamoDbLib.call("get", params);

        if (result.Item) {
            return success(result.Item);
        } else {
            return failure({ status: false, error: "Item not found." });
        }
    } catch (e) {
        return failure({ status: false });
    }
}