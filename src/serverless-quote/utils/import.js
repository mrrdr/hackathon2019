const AWS = require("aws-sdk");
const fs = require('fs');
const path = require('path');

+function main() {
    const dataSourceName = process.argv[2];
    loadItems(dataSourceName);
}();

function loadItems(inputFile) {
    const dynamoDb = new AWS.DynamoDB({ region: 'us-east-1' });
    const client = new AWS.DynamoDB.DocumentClient({ service: dynamoDb });

    const sourcePath = path.resolve(__dirname, `../../../data/${inputFile}.json`);
    const allDocs = JSON.parse(fs.readFileSync(sourcePath, 'utf8'));

    const tableName = `serverless_${inputFile}`;

    allDocs.forEach((doc) => {
        var params = {
            TableName: tableName,
            Item: dataMapper(doc)
        };

        client.put(params, (err, data) => {
            if (err) {
                console.error("Unable to add document", doc, ". Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("PutItem succeeded:", doc);
            }
        });
    });
}

function dataMapper(doc) {
    const obj = {};

    for (const p in doc) {
        obj[p] = doc[p];
    }

    return obj;
};
